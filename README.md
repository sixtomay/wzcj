# WordPress小程序插件

#### 介绍
打通WordPress网站连接微信、QQ、百度、今日头条小程序API插件。

#### 软件架构
基于最强大PHP轻量级语言开发完成。


#### 安装教程

1. 作者官网安装版[点击查看](https://cxcat.com/183.html)
2. 零到一安装版[点击查看](https://blog.csdn.net/qq_37552048/category_11120874.html)

#### 使用说明

1.  域名+服务器，域名备案
2.  ssl证书需要https
3.  搭建一个WordPress网站

#### 推荐服务器

特价实用服务器推荐[点击查看](https://www.kancloud.cn/wzcode/wanzi_miniprogram_install_guide/2293448)
